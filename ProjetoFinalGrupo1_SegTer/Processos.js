const {createApp} = Vue;

createApp({
    data(){
        return{
            password:"",
            email:"",
            success: false,
            error: "",
            userAdmin: false,
            index: null,

            emailCad: null,
            passwordCad: null,
            passwordCadConfirm: null,
            
            estadoLista: false,
            modalState: false,
            cartState:false,

            emailIndex: ["admin@gmail.com","Base@gmail.com","Base2@gmail.com"],
            senhas: ["senhaadmin","2","1"],
            mostrarLogin: true,
            mostrarCadastro: false,
            mostrarConfirmacao: false,

            totalCarrinho: 0,

            cart:[],
            jogosFPS:[
                {
                    name:"DOOM Eternal",
                    price: 129.90,
                    image: "./Midia/Doom_eternal.png",
                    desconto: 0.4,/*% em decimal, ou seja 50% = 0.5*/
                    amount: 0,
                },
                {
                    name:"Counter-Strike: Global Offensive",
                    desconto: 0.5,
                    price: 420.90,
                    image: "./Midia/CSgo.png",
                    amount: 0,
                },
                {
                    name:"Rainbow Six Siege",
                    desconto: 0.65,
                    price: 69.90,
                    image: "./Midia/R6.png",
                    amount: 0,
                },
                {
                    name:"Call of Duty: Black Ops III",
                    desconto: 0.35,
                    price: 69.90,
                    image: "./Midia/CallOfDuty_BlackOps.png",
                    amount: 0,
                },
            ],
            jogosRPG:[
                {
                    name:"Final Fantasy XVI",
                    price: 129.90,
                    image: "./Midia/Jogo1.png",
                    amount: 0,
                },
                {
                    name:"The Legend of Heroes: Trails into Reverie",
                    price: 420.90,
                    image: "./Midia/RPG1.jpg",
                    amount: 0,
                },
                {
                    name:"Destiny 2",
                    price: 69.90,
                    image: "./Midia/RPG2.jpg",
                    amount: 0,
                },
                {
                    name:"ELDEN RING",
                    price: 69.90,
                    image: "./Midia/RPG3.jpg",
                    amount: 0,
                },
            ],
            jogosTerror:[
                {
                    name:"Pacify",
                    price: 129.90,
                    image: "./Midia/Terror1.jpg",
                    amount: 0,
                },
                {
                    name:"Left 4 Dead 2",
                    price: 420.90,
                    image: "./Midia/Terror2.jpg",
                    amount: 0,
                },
                {
                    name:"Devour",
                    price: 69.90,
                    image: "./Midia/Terror3.jpg",
                    amount: 0,
                },
                {
                    name:"Dead by Daylight",
                    price: 69.90,
                    image: "./Midia/Terror4.jpg",
                    amount: 0,
                },
            ],
            jogosFutebol:[
                {
                    name:"New Star Soccer 5",
                    price: 129.90,
                    image: "./Midia/futebol1.jpg",
                    amount: 0,
                },
                {
                    name:"Captain Tsubasa: Rise of New Champions",
                    price: 420.90,
                    image: "./Midia/futebol2.jpg",
                    amount: 0,
                },
                {
                    name:"Save Your Nuts",
                    price: 69.90,
                    image: "./Midia/futebol3.jpg",
                    amount: 0,
                },
                {
                    name:"EA SPORTS™ FIFA 23",
                    price: 69.90,
                    image: "./Midia/futebol4.jpg",
                    amount: 0,
                },
            ],
        }
    },

    computed:{
        cartItemCount(){
            for (let index = 0; index < cart.length; index++) {
                this.totalCarrinho = this.totalCarrinho + this.cart[index].amount;
                
            }
            return this.totalCarrinho;
        },
        cartTotal(){
            return this.cart.reduce((total, product) => total + product.price, 0);
        },
    },

    methods:{
        login(){
            this.userAdmin = false;
            this.index = this.emailIndex.indexOf(this.email);
            localStorage.setItem("indexAtual", this.index);

            if (localStorage.getItem("emailIndex") && localStorage.getItem("senhas")){
                this.emailIndex = JSON.parse(localStorage.getItem("emailIndex"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
                
            }

            setTimeout(() => {
                setTimeout(() => {
                    document.getElementById("btLogin1").style.border = "1.8px solid #8877df";
                    document.getElementById("btLogin2").style.border = "1.8px solid #8877df";
                }, 2500);

                if(this.index !== -1 && this.senhas[this.index] === this.password){
                    this.error = false;
                    this.success = true;
                    this.modalState = false;

                    document.getElementById("btLogin1").style.border = "1.8px solid #ff77df";
                    document.getElementById("btLogin2").style.border = "1.8px solid #ff77df";
                    
                    if (this.index === 0 || this.email === "admin") {
                        this.userAdmin = true;
                        document.getElementById("cadastroADM").style.display="inline";
                        document.getElementById("btLogin1").style.border = "1.8px solid #5dea9c";
                        document.getElementById("btLogin2").style.border = "1.8px solid #5dea9c";
                    }
                }

                else{
                    document.getElementById("btLogin1").style.border = "1.8px solid #FF0000";
                    document.getElementById("btLogin2").style.border = "1.8px solid #FF0000";
                    this.password = "";
                    this.email = "";

                }
            }, 700);
        },

        voltar(){
            window.location.href = 'Main.html'
        },

        adicionarUsuario(){
            this.index = localStorage.getItem("indexAtual");
            setTimeout(() => {
                document.getElementById("btCadastra1").style.border = "1.8px solid #8877df";
                document.getElementById("btCadastra2").style.border = "1.8px solid #8877df";
                document.getElementById("btCadastra3").style.border = "1.8px solid #8877df";
                this.error = null;
            }, 2500);
            setTimeout(() => {
                
                if (!this.emailCad || this.passwordCad.includes(" ")) {
                    this.error = "Nome de usuario ja existente";
                    document.getElementById("btCadastra1").style.border = "1.8px solid #FF0000";
                    document.getElementById("btCadastra2").style.border = "1.8px solid #FF0000";
                    document.getElementById("btCadastra3").style.border = "1.8px solid #FF0000";
                }
                else if(!this.passwordCad || this.passwordCad !== this.passwordCadConfirm || this.passwordCad.includes(" ")){
                    this.error = "As senha precisam ser iguais";
                    document.getElementById("btCadastra3").style.border = "1.8px solid #FF0000";
                    document.getElementById("btCadastra4").style.border = "1.8px solid #FF0000";
                }
                else if(!this.passwordCadConfirm){
                    this.error = "confirme sua senha";
                    document.getElementById("btCadastra4").style.border = "1.8px solid #FF0000";
                }
                else{
                    if (!this.emailIndex.includes(this.emailCad)) {
                        
                        this.emailIndex.push(this.emailCad);
                        this.senhas.push(this.passwordCad);
                        document.getElementById("btCadastra1").style.border = "1.8px solid #ff77df";
                        document.getElementById("btCadastra2").style.border = "1.8px solid #ff77df";
                        document.getElementById("btCadastra3").style.border = "1.8px solid #ff77df";
                        
                        localStorage.setItem("emailIndex", JSON.stringify(this.emailIndex));
                        localStorage.setItem("senhas", JSON.stringify(this.senhas));
                        
                        this.email= this.emailCad
                        this.modalState = false;
                    }
                    else{
                        document.getElementById("bt1").style.border = "1.8px solid #FF0000";
                    }
                    
                }
            }, 700);
        },
        
        ver_cadastrados(){
            this.index = localStorage.getItem("indexAtual");
            this.email = localStorage.getItem("email");
            this.password = localStorage.getItem("password");
            this.estadoLista = !this.estadoLista;

            if (this.index === "0") {
                if(localStorage.getItem("emailIndex") && localStorage.getItem("senhas")){
                    this.emailIndex = JSON.parse(localStorage.getItem("emailIndex"));
                    this.senhas = JSON.parse(localStorage.getItem("senhas"));
                }
            }
        },
        excluirUsuario(email){
            setTimeout(() => {
                this.error = null;
            }, 2500);
            if(email === "admin@gmail.com"){
                this.error = "Este usuario nao pode ser removido";
                return;
            }
            if(confirm("Você tem certeza? Essa ação é inreversivel!")) {
                const index = this.emailIndex.indexOf(email);
                if(index === "-1"){
                    this.error = "Usuario nao encontrado";
                }
                else{
                    this.emailIndex.splice(index, 1);
                    this.senhas.splice(index, 1);
                    
                    localStorage.setItem("emailIndex", JSON.stringify(this.emailIndex));
                    localStorage.setItem("senhas", JSON.stringify(this.senhas));
                }
            }
        },

        modalOpen(pagina){
            this.modalState = !this.modalState;
            switch (pagina){
                case "login": 
                    this.mostrarLogin = true;
                    this.mostrarCadastro = false;
                    this.mostrarConfirmacao = false;

                    document.getElementById("Cadastro").style.backgroundColor = "white"
                    document.getElementById("Login").style.backgroundColor = "rgb(194, 194, 194)"
                break;
            
                case "cadastro":
                    this.mostrarLogin = false;
                    this.mostrarCadastro = true;
                    this.mostrarConfirmacao = false;

                    document.getElementById("Cadastro").style.backgroundColor = "rgb(194, 194, 194)"
                    document.getElementById("Login").style.backgroundColor = "white"
                break;
                case "confirm":
                    this.mostrarLogin = false;
                    this.mostrarCadastro = false;
                    this.mostrarConfirmacao = true;
                break;
            }
        },
        cartOpen(){
            if(this.email){
                this.cartState = !this.cartState;
            }
            else{
                this.modalState = true;
            }
        },
        menuLogin(){
            this.mostrarLogin = true;
            this.mostrarCadastro = false;
            
            document.getElementById("Cadastro").style.backgroundColor = "white"
            document.getElementById("Login").style.backgroundColor = "rgb(194, 194, 194)"
        },
        menuCadastro(){
            this.mostrarCadastro = true;
            this.mostrarLogin = false;
            
            document.getElementById("Cadastro").style.backgroundColor = "rgb(194, 194, 194)"
            document.getElementById("Login").style.backgroundColor = "white"
        },
        gotoAdmin(){
            window.location.href = 'admin.html';
        },
        gotoMain(){
            window.location.href = 'main.html';
        },

        updateProducts(){
            const productId = window.location.hash.split("/")[2];
            const product = this.products.find(item => item.id.toString() === productId);
            this.currentProduct = product ? product : {};
            temAlgo = true;
        },
        addToCart(product){
            product.amount = product.amount + 1; 
            if (product.amount <= 1) {
                this.cart.push(product);
            }
            // for (let index = 0; index < cart.length; index++) {
            //     this.totalCarrinho = this.totalCarrinho + this.cart[index].amount;
            // }
        },
        removeFromCart(product){
            const index = this.cart.indexOf(product);
            if(index != -1){
                this.cart.splice(index, 1);
                product.amount = 0;
            }
        },
    },
}).mount("#main");
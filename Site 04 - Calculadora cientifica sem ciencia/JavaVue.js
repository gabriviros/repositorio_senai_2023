const{createApp} = Vue;

createApp({
    data(){
        return{
            display:"0",
            operador: null,
            numAtual: null,
            numAnterior: null,
            tamanhoLetra: 3 + "vw",
        };
    },    
    methods:{
        btnum(valor){
            if(this.display == "0" || isNaN(this.display)){
                this.display = valor.toString();
            }
            
            else{
                if (this.operador == "="){
                    this.display = "";
                    this.operador = null;                
                }
                this.display += valor.toString();
            }
            this.dispResponsivo();
        },
        btdelete(){
            if(this.display.length >= 2){
                this.display = this.display.slice(0, -1);
            }
            else{
                this.display = 0;
                this.operador = null;
                this.numAtual = null;
                this.numAnterior = null;
            } 
            this.dispResponsivo();
        },
        btdec(){
            if(!this.display.includes(".")){
                this.display += ".";
            }
            this.dispResponsivo();
        },
        btoperacao(operacao){           

            if(this.operador != null){
                const displayAtual = Number(this.display);

                switch (this.operador) {
                    case "+":
                        this.display = ((this.numAtual + displayAtual).toFixed(16) * 1).toString();
                    break;

                    case "-":
                        this.display = ((this.numAtual - displayAtual).toFixed(16) * 1).toString();
                    break;

                    case "*":
                        this.display = ((this.numAtual * displayAtual).toFixed(16) * 1).toString();
                    break;  

                    case "/":
                        this.display = ((this.numAtual / displayAtual).toFixed(16) * 1).toString();
                    break;
                }
                
                this.numAnterior = this.numAtual;
                this.operador = null;
                this.numAtual = null;
                
                if (!isFinite(this.display)){
                    this.display = "Não é possivel";
                }
                if (isNaN(this.display)){
                    this.display = "Impossivel realizar operação";
                }
            }
            
                if(operacao != "="){
                    this.operador = operacao;
                    this.numAtual = parseFloat(this.display);
                    this.display = "0";   
                }
                else{
                    this.operador = operacao;
                }
                this.dispResponsivo();
        },

        dispResponsivo(){
            if(this.display.length >= 18 && this.display.length < 27) {
                this.tamanhoLetra = 2 +"vw";
            }
            else if(this.display.length >= 27 && this.display.length < 36){
                this.tamanhoLetra = 1.5 + "vw";
            }
            else if(this.display.length >= 36){
                this.tamanhoLetra = 1 + "vw";
            }
            else{
                this.tamanhoLetra = 3 + "vw";
            }
        }

    },

}).mount("#app");
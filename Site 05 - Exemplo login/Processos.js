const {createApp} = Vue;

createApp({
    data(){
        return{
            username:"",
            password:"",
            success: false,
            error: false,
            userAdmin: false,
            index: null,

            usernameCad: null,
            passwordCad: null,
            passwordCadConfirm: null,
            
            estadoLista: false,

            usuarios: ["admin","1","2"],
            senhas: ["senhaadmin","2","1"],
        }
    },

    methods:{
        login(){
            this.userAdmin = false;
            this.index = this.usuarios.indexOf(this.username);
            localStorage.setItem("indexAtual", this.index);

            if (localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senhas = JSON.parse(localStorage.getItem("senhas"));
            }

            setTimeout(() => {
                setTimeout(() => {
                    document.getElementById("bt1").style.border = "1.8px solid #8877df";
                    document.getElementById("bt2").style.border = "1.8px solid #8877df";
                }, 2500);

                if(this.index !== -1 && this.senhas[this.index] === this.password){
                    this.error = false;
                    this.success = true;

                    document.getElementById("bt1").style.border = "1.8px solid #ff77df";
                    document.getElementById("bt2").style.border = "1.8px solid #ff77df";
                    
                    if (this.index === 0 || this.username === "admin") {
                        this.userAdmin = true;
                        document.getElementById("cadastroADM").style.display="inline";
                        document.getElementById("bt1").style.border = "1.8px solid #5dea9c";
                        document.getElementById("bt2").style.border = "1.8px solid #5dea9c";
                    }
                }

                else{
                    document.getElementById("bt1").style.border = "1.8px solid #FF0000";
                    document.getElementById("bt2").style.border = "1.8px solid #FF0000";
                    this.password = "";
                    this.username = "";

                }
            }, 700);
        },

        Cadastro(){
            setTimeout(() => {
                if (this.userAdmin == true){
                    window.location.href = 'cadastro.html';
                }
            }, 700);
        },
        voltar(){
            window.location.href = 'Main.html'
        },
        adicionarUsuario(){
            this.index = localStorage.getItem("indexAtual");
            setTimeout(() => {
                document.getElementById("bt1").style.border = "1.8px solid #8877df";
                document.getElementById("bt2").style.border = "1.8px solid #8877df";
                document.getElementById("bt3").style.border = "1.8px solid #8877df";
                this.error = null;
            }, 2500);
            setTimeout(() => {
                
                if (!this.usernameCad || this.passwordCad.includes(" ")) {
                    this.error = "Nome de usuario ja existente";
                    document.getElementById("bt1").style.border = "1.8px solid #FF0000";
                    document.getElementById("bt2").style.border = "1.8px solid #FF0000";
                    document.getElementById("bt3").style.border = "1.8px solid #FF0000";
                }
                else if(!this.passwordCad || this.passwordCad !== this.passwordCadConfirm || this.passwordCad.includes(" ")){
                    this.error = "As senha precisam ser iguais";
                    document.getElementById("bt2").style.border = "1.8px solid #FF0000";
                    document.getElementById("bt3").style.border = "1.8px solid #FF0000";
                }
                else if(!this.passwordCadConfirm){
                    this.error = "confirme sua senha";
                    document.getElementById("bt3").style.border = "1.8px solid #FF0000";
                }
                else{
                    if (this.index === "0"){
                        if (!this.usuarios.includes(this.usernameCad)) {
                            
                            this.usuarios.push(this.usernameCad);
                            this.senhas.push(this.passwordCad);

                            document.getElementById("bt1").style.border = "1.8px solid #ff77df";
                            document.getElementById("bt2").style.border = "1.8px solid #ff77df";
                            document.getElementById("bt3").style.border = "1.8px solid #ff77df";
                            
                            localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                            localStorage.setItem("senhas", JSON.stringify(this.senhas));
                        }
                        else{
                            document.getElementById("bt1").style.border = "1.8px solid #FF0000";
                        }
                    }
                    else{
                        document.getElementById("bt1").style.border = "1.8px solid #FFFFFF";
                        document.getElementById("bt2").style.border = "1.8px solid #FFFFFF";
                        document.getElementById("bt3").style.border = "1.8px solid #FFFFFF";
                    }       
                }
            }, 700);
        },
        ver_cadastrados(){
            this.index = localStorage.getItem("indexAtual");
            this.username = localStorage.getItem("username");
            this.password = localStorage.getItem("password");
            
            this.estadoLista = !this.estadoLista;

            if (this.index === "0") {
                if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                    this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                    this.senhas = JSON.parse(localStorage.getItem("senhas"));
                }
            }
        },
        excluirUsuario(usuario){
            setTimeout(() => {
                this.error = null;
            }, 2500);
            if(usuario === "admin"){
                this.error = "Este usuario nao pode ser removido";
                return;
            }
            if(confirm("Você tem certeza? Essa ação é inreversivel!")) {
                const index = this.usuarios.indexOf(usuario);
                if(index === "-1"){
                    this.error = "Usuario nao encontrado";
                }
                else{
                    this.usuarios.splice(index, 1);
                    this.senhas.splice(index, 1);
                    
                    localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                    localStorage.setItem("senhas", JSON.stringify(this.senhas));
                }
            }

        },
    },
    
}).mount("#app");
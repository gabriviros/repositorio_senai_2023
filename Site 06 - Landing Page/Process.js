const {createApp} = Vue;

createApp({
    data(){
        return{
            currentProduct:[],
            cart:[],
            
            products:[
                {
                    id: 1,
                    name:"Tênis",
                    description:"Um par de tênis que definitivamente é um tênis",
                    price: 129.90,
                    image: "./imagens/imagemLegal.png",
                },
                {
                    id: 2,
                    name:"Botas",
                    description:"As lendarias botas do Gato de Botas",
                    price: 420.90,
                    image: "./imagens/imagemBotas.png",
                },
                {
                    id: 3,
                    name:"Sapato",
                    description:"Um par de sapatos que te faz parecer descalço",
                    price: 69.90,
                    image: "./imagens/imagemSapato.png",
                },
                {
                    id: 4,
                    name:"Sandalia",
                    description:"Um par de sandalia perfeito para sair de casa",
                    price: 49.90,
                    image: "./imagens/imagemSandalia.png",
                },
            ],
        }
    },
    
    mounted(){
        window.addEventListener("hashchange",this.updateProducts);
        this.updateProducts();
    },

    //Função do vue para retornar os dados especificos de um bloco programado

    computed:{
        cartItemCount(){
            return this.cart.length;
        },
        cartTotal(){
            return this.cart.reduce((total, product) => total + product.price, 0);
        },
    },

    methods:{
        updateProducts(){
            const productId = window.location.hash.split("/")[2];
            const product = this.products.find(item => item.id.toString() === productId);
            this.currentProduct = product ? product : {};
            temAlgo = true;
        },
        addToCart(product){
            this.cart.push(product)
        },
        removeFromCart(product){
            const index = this.cart.indexOf(product);
            if(index != -1){
            this.cart.splice(index, 1);
            }
        },
    }
}).mount("#app");